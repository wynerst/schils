<?php
/**
 *
 * Copyright (C) 2010  Arie Nugraha (dicarve@yahoo.com)
 * Modified for Excel output (C) 2010 by Wardiyono (wynerst@gmail.com)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Bukti Fisik Pustakawan */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';
// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');
// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

require SIMBIO.'simbio_GUI/template_parser/simbio_template_parser.inc.php';
require SIMBIO.'simbio_GUI/table/simbio_table.inc.php';
require SIMBIO.'simbio_GUI/paging/simbio_paging.inc.php';
require SIMBIO.'simbio_GUI/form_maker/simbio_form_element.inc.php';
require SIMBIO.'simbio_DB/datagrid/simbio_dbgrid.inc.php';
require MDLBS.'reporting/report_dbgrid.inc.php';

$page_title = 'Laporan Bukti Fisik';
$reportView = false;
$num_recs_show = 20;
if (isset($_GET['reportView'])) {
    $reportView = true;
}

if (!$reportView) {
?>
    <!-- filter -->
    <fieldset>
    <div class="per_title">
    	<h2><?php echo __('Bukti Fisik Pustakawan'); ?></h2>
	  </div>
    <div class="infoBox">
    <?php echo __('Report Filter'); ?>
    </div>
    <div class="sub_section">
    <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>" target="reportView">
    <div id="filterForm">
        
        <div class="divRow">
            <div class="divRowLabel"><?php echo __('Jenis Pekerjaan'); ?></div>
            <div class="divRowContent">
            <?php
            $gender_chbox[0] = array('ALL', __('ALL'));
            $gender_chbox[1] = array('circulation', __('CIRCULATION'));
            $gender_chbox[2] = array('bibliography', __('BIBLIOGRAPHY'));
			$gender_chbox[3] = array('membership', __('MEMBERSHIP'));
			$gender_chbox[4] = array('system', __('SYSTEM'));
			$gender_chbox[5] = array('stock_take', __('STOCK TAKE'));
            echo simbio_form_element::radioButton('location', $gender_chbox, 'ALL');
            ?>
            </div>
        </div>
	   <div class="divRow">
            <div class="divRowLabel"><?php echo __('Nama Pustakawan'); ?></div>
            <div class="divRowContent">
            <?php echo simbio_form_element::textField('text', 'keterangan1', '', 'style="width: 50%"'); ?>
            </div>
        </div>
        <div class="divRow">
            <div class="divRowLabel"><?php echo __('Item Pekerjaan'); ?></div>
            <div class="divRowContent">
            <?php echo simbio_form_element::textField('text', 'keterangan2', '', 'style="width: 50%"'); ?>
            </div>
        </div>
        <div class="divRow">
            <div class="divRowLabel"><?php echo __('Tanggal Mulai'); ?></div>
            <div class="divRowContent">
            <?php echo simbio_form_element::dateField('startDate', '2017-01-01'); ?>
            </div>
        </div>
        <div class="divRow">
            <div class="divRowLabel"><?php echo __('Tanggal Akhir'); ?></div>
            <div class="divRowContent">
            <?php echo simbio_form_element::dateField('untilDate', date('Y-m-d')); ?>
            </div>
        </div>
        <div class="divRow">
            <div class="divRowLabel"><?php echo __('Record each page'); ?></div>
            <div class="divRowContent"><input type="text" name="recsEachPage" size="3" maxlength="3" value="<?php echo $num_recs_show; ?>" /> <?php echo __('Set between 20 and 200'); ?></div>
        </div>
    </div>
    <div style="padding-top: 10px; clear: both;">
    <input type="button" name="moreFilter" value="<?php echo __('Show More Filter Options'); ?>" />
    <input type="submit" name="applyFilter" value="<?php echo __('Apply Filter'); ?>" />
    <input type="hidden" name="reportView" value="true" />
    </div>
    </form>
	</div>
    </fieldset>
    <!-- filter end -->
    <div class="dataListHeader" style="padding: 3px;"><span id="pagingBox"></span></div>
    <iframe name="reportView" id="reportView" src="<?php echo $_SERVER['PHP_SELF'].'?reportView=true'; ?>" frameborder="0" style="width: 100%; height: 500px;"></iframe>
<?php
} else {
    ob_start();
    // table spec
    $table_spec = 'system_log AS ls';
    
    // create datagrid
    $reportgrid = new report_datagrid();
    $reportgrid->setSQLColumn('IF(ls.log_id IS NOT NULL, ls.log_id, \'NON-MEMBER\')  AS \''.__('Member ID').'\'',
        'ls.log_location AS \''.__('Jenis Pekerjaan').'\'',
        'ls.log_msg AS \''.__('Keterangan').'\'',
        'ls.log_date AS \''.__('Tanggal Transaksi').'\'');
    $reportgrid->setSQLorder('ls.log_id ASC');

    // is there any search
    $criteria = 'ls.log_id IS NOT NULL ';
   
     if (isset($_GET['location']) AND $_GET['location']<>'ALL' ) {
        $location= $dbs->escape_string(trim($_GET['location']));
        $criteria .= ' AND ls.log_location=\''.$location.'\'';
    } 
	 if (isset($_GET['keterangan1']) AND !empty($_GET['keterangan1'])) {
        $keterangan1= $dbs->escape_string(trim($_GET['keterangan1']));
        $criteria .= ' AND ls.log_msg LIKE \'%'.$keterangan1.'%\'';
		
    }
	 if (isset($_GET['keterangan2']) AND !empty($_GET['keterangan2'])) {
        $keterangan2= $dbs->escape_string(trim($_GET['keterangan2']));
        $criteria .= ' AND ls.log_msg LIKE \'%'.$keterangan2.'%\'';
    }
    // register date
    if (isset($_GET['startDate']) AND isset($_GET['untilDate'])) {
        $criteria .= ' AND (TO_DAYS(ls.log_date) BETWEEN TO_DAYS(\''.$_GET['startDate'].'\') AND
            TO_DAYS(\''.$_GET['untilDate'].'\'))';
    }
    if (isset($_GET['recsEachPage'])) {
        $recsEachPage = (integer)$_GET['recsEachPage'];
        $num_recs_show = ($recsEachPage >= 20 && $recsEachPage <= 200)?$recsEachPage:$num_recs_show;
    }
    $reportgrid->setSQLCriteria($criteria);

    // put the result into variables
    echo $reportgrid->createDataGrid($dbs, $table_spec, $num_recs_show);

    echo '<script type="text/javascript">'."\n";
    echo 'parent.$(\'#pagingBox\').html(\''.str_replace(array("\n", "\r", "\t"), '', $reportgrid->paging_set).'\');'."\n";
    echo '</script>';
	$xlsquery = 'SELECT IF(ls.log_id IS NOT NULL, ls.log_id, \'NON-MEMBER\')  AS \''.__('Log ID').'\''.
        ', ls.log_location AS \''.__('Location').'\''.
        ', ls.log_msg AS \''.__('Keterangan').'\''.
        ', ls.log_date AS \''.__('Log Date').'\''.
		' FROM '.$table_spec.' WHERE '.$criteria. ' ORDER BY ls.log_id ASC';

		unset($_SESSION['xlsdata']); 
		$_SESSION['xlsquery'] = $xlsquery;
		$_SESSION['tblout'] = "system_log";

	echo '<p><a href="../xlsoutput.php" class="button">'.__('Export to spreadsheet format').'</a></p>';

    $content = ob_get_clean();
    // include the page template
    require SB.'/admin/'.$sysconf['admin_template']['dir'].'/printed_page_tpl.php';
}
