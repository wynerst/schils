<?php
/**
 *
 * Copyright (C) 2017  Wardiyono (wynerst@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* School Profile */

// key to authenticate
define('INDEX_AUTH', '1');
// key to get full database access
define('DB_ACCESS', 'fa');

if (!defined('SB')) {
    // main system configuration
    require '../../../sysconfig.inc.php';
    // start the session
    require SB.'admin/default/session.inc.php';
}
// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-system');

// only administrator have privileges to change global settings
if ($_SESSION['uid'] != 1) {
    header('Location: '.MWB.'system/content.php');
    die();
}

require SB.'admin/default/session_check.inc.php';
require SIMBIO.'simbio_FILE/simbio_directory.inc.php';
require SIMBIO.'simbio_GUI/form_maker/simbio_form_table_AJAX.inc.php';
require SIMBIO.'simbio_GUI/table/simbio_table.inc.php';
require SIMBIO.'simbio_DB/simbio_dbop.inc.php';

?>
<fieldset class="menuBox">
  <div class="menuBoxInner systemIcon">
    <div class="per_title">
      <h2><?php echo __('Node Profile for UCS server'); ?></h2>
    </div>
    <div class="infoBox">
      <?php echo __('Modify node profile'); ?>
    </div>
  </div>
</fieldset>
<?php
/* main content */
/* Config Vars EDIT FORM */
/* Config Vars update process */

if (isset($_POST['updateData'])) {
    // reset/truncate setting table content
    // school id
    //$ucnode_id = $dbs->escape_string(strip_tags(trim($_POST['ucnode_id'])));
    //$dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($ucnode_id)).'\' WHERE setting_name=\'ucnode_id\'');

    // school server
    //$ucnode_server = $dbs->escape_string(strip_tags(trim($_POST['ucnode_server'])));
    //$dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($ucnode_server)).'\' WHERE setting_name=\'ucnode_server\'');

    // school name
    $ucnode_name = $dbs->escape_string(strip_tags(trim($_POST['ucnode_name'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($ucnode_name)).'\' WHERE setting_name=\'ucnode_name\'');

    // node password
	if ($_POST['ucnode_pass'] <> "") {
      $ucnode_pass = $dbs->escape_string(strip_tags(trim($_POST['ucnode_pass'])));
	} else {
	  $ucnode_pass = SHA1($sysconf['school_id']);
	}
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($ucnode_pass)).'\' WHERE setting_name=\'ucnode_pass\'');

    // write log
    utility::writeLogs($dbs, 'staff', $_SESSION['uid'], 'system', $_SESSION['realname'].' change ucs node profile');
    utility::jsAlert(__('Profile saved. Refreshing page'));
    echo '<script type="text/javascript">parent.location.href = \'../../index.php?mod=system\';</script>';
}
/* Config Vars update process end */

// create new instance
$form = new simbio_form_table_AJAX('mainForm', $_SERVER['PHP_SELF'], 'post');
$form->submit_button_attr = 'name="updateData" value="'.__('Save Settings').'" class="btn btn-default"';

// form table attributes
$form->table_attr = 'align="center" id="dataList" cellpadding="5" cellspacing="0"';
$form->table_header_attr = 'class="alterCell" style="font-weight: bold;"';
$form->table_content_attr = 'class="alterCell2"';

// load settings from database
utility::loadSettings($dbs);

// version status
$form->addAnything('Senayan Version', '<strong>'.SENAYAN_VERSION.'</strong>');

// UCS server
$form->addTextField('text', 'ucnode_server', __('Target/Host server'), $sysconf['ucnode_server'], 'style="width: 50%;" disabled=""');
// school name
$form->addTextField('text', 'ucnode_id', __('School Id'), $sysconf['school_id'], 'style="width: 50%;" disabled=""');

if (isset($sysconf['ucnode_name']) AND $sysconf['ucnode_name']<>"") {
  $node_name = $sysconf['ucnode_name'];	
} else {
  $node_name = $sysconf['school_name'];
}

// library subname
$form->addTextField('text', 'ucnode_name', __('School Name'), $node_name, 'style="width: 95%;"');

if (isset($sysconf['ucnode_pass']) and $sysconf['ucnode_pass']<>"") {
  $node_pass = $sysconf['ucnode_pass'];	
} else {
  $node_pass = SHA1($sysconf['school_id']);
}

// ucnode password
$form->addTextField('text', 'ucnode_pass', __('Node Password'), $node_pass, 'style="width: 75%;"');

// print out the object
echo $form->printOut();

// server settings
?>
<table id="dataList" align="center" cellspacing="0" cellpadding="5">
<tbody>
<tr row="1" style="cursor: pointer;"><td class="alterCell" style="font-weight: bold;" width="80%" valign="top">
<?php echo __('Please send the settings bellow to Union Catalog Server Administrator'); ?>
</td></tr>
<tr row="0" style="cursor: pointer;"><td class="alterCell" style="font-weight: bold;" width="80%" valign="top">
// nodes configuration<br />
$sysconf['node']['<?php echo $sysconf['school_id'];?>'] = array(<br />
&nbsp;&nbsp;&nbsp;&nbsp;'id' => '<?php echo $sysconf['school_id'];?>', // node id (must be UNIQUE, lowercase and not containing any spaces!)<br />
&nbsp;&nbsp;&nbsp;&nbsp;'name' => '<?php echo $sysconf['ucnode_name'];?>', // node name<br />
&nbsp;&nbsp;&nbsp;&nbsp;'password' => '<?php echo $sysconf['ucnode_pass'];?>', // this hash created with SHA1 algoritm<br />
&nbsp;&nbsp;&nbsp;&nbsp;'baseurl' => '<?php echo 'http://'.$_SERVER['SERVER_NAME'].SWB ;?>', // node base URL<br />
&nbsp;&nbsp;&nbsp;&nbsp;'ip' => '' // IP address of node<br />
&nbsp;&nbsp;&nbsp;&nbsp;);<br/>
</td></tr>
</tbody></table>
<?php
/* main content end */
