<?php
/**
 *
 * Copyright (C) 2017  Wardiyono (wynerst@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* School Profile */

// key to authenticate
define('INDEX_AUTH', '1');
// key to get full database access
define('DB_ACCESS', 'fa');

if (!defined('SB')) {
    // main system configuration
    require '../../../sysconfig.inc.php';
    // start the session
    require SB.'admin/default/session.inc.php';
}
// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-system');

// only administrator have privileges to change global settings
if ($_SESSION['uid'] != 1) {
    header('Location: '.MWB.'system/content.php');
    die();
}

require SB.'admin/default/session_check.inc.php';
require SIMBIO.'simbio_FILE/simbio_directory.inc.php';
require SIMBIO.'simbio_GUI/form_maker/simbio_form_table_AJAX.inc.php';
require SIMBIO.'simbio_GUI/table/simbio_table.inc.php';
require SIMBIO.'simbio_DB/simbio_dbop.inc.php';

?>
<fieldset class="menuBox">
  <div class="menuBoxInner systemIcon">
    <div class="per_title">
      <h2><?php echo __('School Profile'); ?></h2>
    </div>
    <div class="infoBox">
      <?php echo __('Modify school profile'); ?>
    </div>
  </div>
</fieldset>
<?php
/* main content */
/* Config Vars EDIT FORM */
/* Config Vars update process */

if (isset($_POST['updateData'])) {
    // reset/truncate setting table content
    // school id
    $school_id = $dbs->escape_string(strip_tags(trim($_POST['school_id'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($school_id)).'\' WHERE setting_name=\'school_id\'');

    // school name
    $school_name = $dbs->escape_string(strip_tags(trim($_POST['school_name'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($school_name)).'\' WHERE setting_name=\'school_name\'');

    // school address
    $sch_address = $dbs->escape_string(strip_tags(trim($_POST['sch_address'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_address)).'\' WHERE setting_name=\'sch_address\'');

    // school zip
    $sch_zip = $dbs->escape_string(strip_tags(trim($_POST['sch_zip'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_zip)).'\' WHERE setting_name=\'sch_zip\'');

    // school phone
    $sch_phone = $dbs->escape_string(strip_tags(trim($_POST['sch_phone'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_phone)).'\' WHERE setting_name=\'sch_phone\'');

    // school fax
    $sch_fax = $dbs->escape_string(strip_tags(trim($_POST['sch_fax'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_fax)).'\' WHERE setting_name=\'sch_fax\'');

    // school email
    $sch_email = $dbs->escape_string(strip_tags(trim($_POST['sch_email'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_email)).'\' WHERE setting_name=\'sch_email\'');

    // school www
    $sch_www = $dbs->escape_string(strip_tags(trim($_POST['sch_www'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_www)).'\' WHERE setting_name=\'sch_www\'');

    // school headmaster
    $sch_headmaster = $dbs->escape_string(strip_tags(trim($_POST['sch_headmaster'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_headmaster)).'\' WHERE setting_name=\'sch_headmaster\'');

    // school headmaster-id
    $sch_head_id = $dbs->escape_string(strip_tags(trim($_POST['sch_head_id'])));
    $dbs->query('UPDATE setting SET setting_value=\''.$dbs->escape_string(serialize($sch_head_id)).'\' WHERE setting_name=\'sch_head_id\'');

    // write log
    utility::writeLogs($dbs, 'staff', $_SESSION['uid'], 'system', $_SESSION['realname'].' change school profile');
    utility::jsAlert(__('Profile saved. Refreshing page'));
    echo '<script type="text/javascript">parent.location.href = \'../../index.php?mod=system\';</script>';
}
/* Config Vars update process end */

// create new instance
$form = new simbio_form_table_AJAX('mainForm', $_SERVER['PHP_SELF'], 'post');
$form->submit_button_attr = 'name="updateData" value="'.__('Save Settings').'" class="btn btn-default"';

// form table attributes
$form->table_attr = 'align="center" id="dataList" cellpadding="5" cellspacing="0"';
$form->table_header_attr = 'class="alterCell" style="font-weight: bold;"';
$form->table_content_attr = 'class="alterCell2"';

// load settings from database
utility::loadSettings($dbs);

// version status
$form->addAnything('Senayan Version', '<strong>'.SENAYAN_VERSION.'</strong>');

// library name
$form->addTextField('text', 'school_id', __('School Id'), $sysconf['school_id'], 'style="width: 95%;"');

// library subname
$form->addTextField('text', 'school_name', __('School Name'), $sysconf['school_name'], 'style="width: 95%;"');

// school address
$form->addTextField('text', 'sch_address', __('School address'), $sysconf['sch_address'], 'style="width: 75%;"');

// zip code
$form->addTextField('text', 'sch_zip', __('Zip code'), $sysconf['sch_zip'], 'style="width: 15%;"');
// phone line number
$form->addTextField('text', 'sch_phone', __('School phone no'), $sysconf['sch_phone'], 'style="width: 15%;"');
// fax number
$form->addTextField('text', 'sch_fax', __('School fax no'), $sysconf['sch_fax'], 'style="width: 15%;"');
// email
$form->addTextField('text', 'sch_email', __('Email'), $sysconf['sch_email'], 'style="width: 25%;"');
// web site
$form->addTextField('text', 'sch_www', __('School website'), $sysconf['sch_www'], 'style="width: 25%;"');

// head master
$form->addTextField('text', 'sch_headmaster', __('Headmaster/Principal'), $sysconf['sch_headmaster'], 'style="width: 25%;"');
// head master id
$form->addTextField('text', 'sch_head_id', __('Headmaster ID'), $sysconf['sch_head_id'], 'style="width: 25%;"');

// print out the object
echo $form->printOut();
/* main content end */
