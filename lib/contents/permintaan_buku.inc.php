<!DOCTYPE html>
<html>
<head>
</head>
<body>
<td><center><h4>FORM PERMINTAAN BUKU PERPUSTAKAAN</h4></center></td>
<td><h4>MASUKKAN IDENTITAS ANDA</h4></td>
<form name="permintaan_buku" action = "permintaan_buku_insert.php" method = "post"> 
<table height="100%" border="0" cellpadding="5" cellspacing="0">
	<tbody>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><input type="text" name="nama" maxlength="30" required="required" /></td>
        </tr>
        <tr>
            <td>NIS</td>
            <td>:</td>
            <td><input type="text" name="nim" maxlength="30" required="required" /></td>
        </tr>
        <tr>
            <td>Kelas</td>
            <td>:</td>
            <td><input type="text" name="fakultas" required="required" /></td>
        </tr>
		<tr>
            <td>No. Hp</td>
            <td>:</td>
            <td><input type="text" name="no_hp" maxlength="14" required="required" /></td>
        </tr>
		<tr>
		<td><h4>MASUKKAN DATA BUKU</h4></td>
		</tr>
        <tr>
            <td>Judul Buku</td>
            <td>:</td>
            <td><input type="text" name="judul" required="required" /></td>
        </tr>
        <tr>
            <td>Pengarang</td>
            <td>:</td>
            <td><input type="text" name="pengarang" required="required" /></td>
        </tr>
        <tr>
            <td>Penerbit</td>
            <td>:</td>
            <td><input type="text" name="penerbit" required="required" /></td>
        </tr>
		<tr>
            <td>Tahun Terbit</td>
            <td>:</td>
            <td><input type="text" name="tahun" maxlength="4" required="required" /></td>
        </tr>
        <tr>
            <td align="right" colspan="3"><input type="submit" id="button" value="Kirim"><input type = "reset"  value = "Kosongkan"/></td>
        </tr>
	</tbody>
</table>
</form>
<a href="index.php?p=permintaan_buku_view">Lihat Data</a>
</body>
</html>
