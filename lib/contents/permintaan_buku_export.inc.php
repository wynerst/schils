<?php
// Header
header("Content-type: application/vnd-ms-excel");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma: public");
header("Expires: 0");
 
// Nama file ekspor "PermintaanBuku-export.xls"
header("Content-Disposition: attachment; filename=PermintaanBuku-export.xls");
header("Content-Transfer-Encoding: binary ");
 
// Table
include 'permintaan_buku_view.inc.php';
?>	