<?php
/**
 *
 * Member Area/Information
 * Copyright (C) 2009  Arie Nugraha (dicarve@yahoo.com)
 * Patched by Hendro Wicaksono (hendrowicaksono@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
  die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
  die("can not access this file directly");
}

$icon['fb'] = 'mif-facebook';
$icon['tw'] = 'mif-twitter';
$icon['li'] = 'mif-linkedin';
$icon['pn'] = 'mif-pinterest';
$icon['gp'] = 'mif-google-plus';
$icon['yt'] = 'mif-youtube-play';
$icon['bl'] = 'mif-earth';
$icon['ym'] = 'mif-none';
$icon['rd'] = 'mif-none';

$info = __('Profile of our Librarian');
$page_title = __('Librarian');
// query librarian data
$librarian_q = $dbs->query('SELECT * FROM user WHERE user_type IN (1,2) ORDER BY user_type DESC LIMIT 20');
if ($librarian_q->num_rows > 0) {
  while ($librarian = $librarian_q->fetch_assoc()) {
    if ($librarian['user_image']) {
      $img = SWB.'images/persons/'.$librarian['user_image'];
      if (!file_exists($img)) {
        $img = $sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/assets/images/boss.png';
      }
    } else {
      $img = $sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/assets/images/boss.png';
    }
    ?>
    <div class="tile tile-librarian">
      <div class="tile-content slide-down-2 ">
         <div class="slide">
            <div class="image-container image-format-square" style="width: 100%;">
               <div class="frame">
                  <div style="width: 100%; height: 100%; background-image: url(&quot;<?php echo $img;?>&quot;); background-size: cover; background-repeat: no-repeat; border-radius: 0px;"></div>
               </div>
            </div>
         </div>
         <div class="slide-over bg-blue fg-white text-small padding10 ">
           <div><span class="mif-mail"></span> <?php echo $librarian['email']; ?></div>
           <?php
           $social = array();
           if ($librarian['social_media']){
             $social = @unserialize($librarian['social_media']);
             echo '<ul class="inline-list icon-list no-margin no-padding">';
             foreach ($sysconf['social'] as $id => $social_media) {
               if (isset($social[$id])) {
                 echo '<li><span class="'.$icon[$id].'"></span> '.$social[$id].'</li>';
               }
             }
             echo '</ul>';
           }
           ?>
         </div>
         <div class="tile-label">
           <?php echo $librarian['realname']; ?>
           <div class="text-small"><?php echo $sysconf['system_user_type'][$librarian['user_type']]; ?></div>
         </div>
      </div>
    </div>
    <?php
  }
} else {
  echo '<p>No Librarian data yet</p>';
}
