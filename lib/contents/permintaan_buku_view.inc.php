<h4><center>DATA PERMINTAAN BUKU</center></h4>
<a href="index.php?p=permintaan_buku">+ Tambah Data</a>
<p><a href="index.php?p=permintaan_buku_export">Export to spreadsheet format</a></p>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tdead>
        <tr>
            <td><b><center>No.</center></b></td>
            <td><b><center>Nama</center></b></td>
            <td><b><center>NIM</center></b></td>
            <td><b><center>Fakultas</center></b></td>
            <td><b><center>No. HP</center></b></td>
            <td><b><center>Judul Buku</center></b></td>
            <td><b><center>Pengarang</center></b></td>
		    <td><b><center>Penerbit</center></b></td>
            <td><b><center>Tahun Terbit</center></b></td>

        </tr>
    </tdead>
    <tbody>
	<?php
    $query = $dbs->query("select * from usulan");

    if($query) {
    $no = 1;
    while ($data = $query->fetch_assoc()) {
    ?>
        <tr>
            <td><center><?php echo $no; ?></center></td>
            <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['nim']; ?></td>
            <td><?php echo $data['fakultas']; ?></td>
            <td><?php echo $data['no_hp']; ?></td>
            <td><?php echo $data['judul']; ?></td>
            <td><?php echo $data['pengarang']; ?></td>
			<td><?php echo $data['penerbit']; ?></td>
            <td><?php echo $data['tahun']; ?></td>

        </tr>
    <?php
        $no++;
    }
    } // end if
    ?>
    </tbody>
</table>
