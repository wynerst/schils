<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-25T08:14:02+07:00
# @Email:  ido.alit@gmail.com
# @Filename: 404.inc.php
# @Last modified by:   user
# @Last modified time: 2017-11-25T14:09:41+07:00

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
    die("can not access this file directly");
}

// start the output buffering for main content
ob_start();

?>

<div class="_404">
  <div class="_404_wraper">
    <div class="_404_header">
      404
    </div>
    <div class="_404_content">
      <h1>Sorry!</h1>
      <p>The page you're looking for was not found</p>
    </div>
    <div class="_404_footer">
      <a href="index.php">Go Home</a>
    </div>
  </div>
</div>

<?php
// main content
$main_content = ob_get_clean();
// page title
$page_title = __('Error 404 | Page Not Found');
require_once $sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/404.inc.php';
http_response_code(404);
exit();
