/**
 * @Author: Waris Agung Widodo <user>
 * @Date:   2017-11-16T21:47:16+07:00
 * @Email:  ido.alit@gmail.com
 * @Filename: script.js
 * @Last modified by:   user
 * @Last modified time: 2017-11-29T13:12:33+07:00
 */

 (function($) {
     $.StartScreen = function(){
         var plugin = this;
         var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

         plugin.init = function(){
             setTilesAreaSize();
             if (width > 640) addMouseWheel();
         };

         var setTilesAreaSize = function(){
             var groups = $(".tile-group");
             var tileAreaWidth = 80;
             $.each(groups, function(i, t){
                 if (width <= 640) {
                     tileAreaWidth = width;
                 } else {
                     tileAreaWidth += $(t).outerWidth() + 80;
                 }
             });
             $(".tile-area").css({
                 width: tileAreaWidth
             });
         };

         var addMouseWheel = function (){
             $("body").mousewheel(function(event, delta, deltaX, deltaY){
                 var page = $(document);
                 var scroll_value = delta * 50;
                 page.scrollLeft(page.scrollLeft() - scroll_value);
                 return false;
             });
         };

         plugin.init();
     }

 })(jQuery);

 $(function(){
     $.StartScreen();

     var tiles = $(".tile, .tile-small, .tile-sqaure, .tile-wide, .tile-large, .tile-big, .tile-super");

     $.each(tiles, function(){
         var tile = $(this);
         setTimeout(function(){
             tile.css({
                 opacity: 1,
                 "-webkit-transform": "scale(1)",
                 "transform": "scale(1)",
                 "-webkit-transition": ".3s",
                 "transition": ".3s"
             });
         }, Math.floor(Math.random()*500));
     });

     $(".tile-group").animate({
         left: 0
     });
 });

 function showCharms(id){
     var  charm = $(id).data("charm");
     if (charm.element.data("opened") === true) {
         charm.close();
     } else {
         charm.open();
     }
 }

 function setSearchPlace(el){
     var a = $(el);
     var text = a.text();
     var toggle = a.parents('label').children('.dropdown-toggle');

     toggle.text(text);
 }

 $(function(){
     var current_tile_area_scheme = localStorage.getItem('tile-area-scheme') || "tile-area-scheme-dark";
     $(".tile-area").removeClass (function (index, css) {
         return (css.match (/(^|\s)tile-area-scheme-\S+/g) || []).join(' ');
     }).addClass(current_tile_area_scheme);

     $(".schemeButtons .button").hover(
             function(){
                 var b = $(this);
                 var scheme = "tile-area-scheme-" +  b.data('scheme');
                 $(".tile-area").removeClass (function (index, css) {
                     return (css.match (/(^|\s)tile-area-scheme-\S+/g) || []).join(' ');
                 }).addClass(scheme);
             },
             function(){
                 $(".tile-area").removeClass (function (index, css) {
                     return (css.match (/(^|\s)tile-area-scheme-\S+/g) || []).join(' ');
                 }).addClass(current_tile_area_scheme);
             }
     );

     $(".schemeButtons .button").on("click", function(){
         var b = $(this);
         var scheme = "tile-area-scheme-" +  b.data('scheme');

         $(".tile-area").removeClass (function (index, css) {
             return (css.match (/(^|\s)tile-area-scheme-\S+/g) || []).join(' ');
         }).addClass(scheme);

         current_tile_area_scheme = scheme;
         localStorage.setItem('tile-area-scheme', scheme);

         showSettings();
     });
 });

 $(document).ready(function () {

   $('#simplySearchForm').on('submit', function (e) {
     var keywords = JSON.parse(localStorage.getItem('keywords')),
            keyword = $('#keyword').val()
     if (keywords === undefined || keywords === null) {
       keywords = []
     }
     console.log(keywords)
     if (keyword.trim() !== '') {
       keywords.push(keyword)
       localStorage.setItem('keywords', JSON.stringify(keywords))
     }
   })

   // responsive class
   $.each($('.tile-group'), function (index, value) {
     fixHeight(value)
   })

   function fixHeight(g) {
     g = $(g)
     h = g.height() + 35
     wh = $(document).height() - g.offset().top
     moreThan(h , wh, (res)=> {
       if (res) {
         checkClass(g, () => {
           setTilesAreaSize()
         })
       }
     })
   }

   function setTilesAreaSize(){
       var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
       var groups = $(".tile-group");
       var tileAreaWidth = 80;
       $.each(groups, function(i, t){
           if (width <= 640) {
               tileAreaWidth = width;
           } else {
               tileAreaWidth += $(t).outerWidth() + 80;
           }
       });
       $(".tile-area").css({
           width: tileAreaWidth
       });
   };

   function checkClass(e, call) {
     e = $(e)
     if (e.hasClass('one')) {
       fixClass(e, 'one')
     } else if (e.hasClass('double')) {
       fixClass(e, 'double')
     } else if (e.hasClass('three')) {
       fixClass(e, 'three')
     } else if (e.hasClass('four')) {
       fixClass(e, 'four')
     } else if (e.hasClass('five')) {
       fixClass(e, 'five')
     }

     call()
   }

   function fixClass(e, c) {
     // remove origin class
     e = $(e)
     e.removeClass(c)
     // increment class
     textToInt(c, (r) => {
       intToText(r+2, (n) => {
         e.addClass(n)
       })
     })
   }

   function textToInt(text, call) {
     var r;
     switch (text) {
       case 'one':
         r = 1;
         break;
       case 'double':
         r = 2;
         break;
       case 'three':
         r = 3;
         break;
       case 'four':
         r = 4;
         break;
       case 'five':
         r = 5;
         break;
       default:
         r = null
     }

     call(r)
   }

   function intToText(number, call) {
     var r;
     switch (number) {
       case 1:
         r = 'one';
         break;
       case 2:
         r = 'double';
         break;
       case 3:
         r = 'three';
         break;
       case 4:
         r = 'four';
         break;
       case 5:
         r = 'five';
         break;
       default:
         r = null
     }

     call(r)
   }

   function moreThan(hight, low, call) {
     if (hight > low) {
       call(true)
     } else {
       call(false)
     }
   }

 })
