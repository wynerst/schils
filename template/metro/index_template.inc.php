<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-16T18:49:12+07:00
# @Email:  ido.alit@gmail.com
# @Filename: index_template.inc.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T12:00:38+07:00

define('THEME_DIR', $sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/');

require_once 'tinfo.inc.php';

require_once __DIR__ . '/metro.php';
$metro = new Metro($dbs);

require_once __DIR__ . '/slims.php';
$slims = new Slims($dbs);

if(isset($_GET['search']) || isset($_GET['title']) || isset($_GET['keywords']) || isset($_GET['p'])) {

  // main content
  include_once 'parts/portal.php';

} else {
  // start screen
  include_once 'parts/start_screen.php';
}
?>
