<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-16T21:52:54+07:00
# @Email:  ido.alit@gmail.com
# @Filename: login_template.inc.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T12:21:06+07:00

define('THEME_DIR', $sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/');

include 'tinfo.inc.php';

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
    <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

    <link rel='shortcut icon' type='image/x-icon' href='webicon.ico' />
    <title><?php echo $page_title; ?></title>

    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-colors.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-icons.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-responsive.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>assets/css/style_portal.css">

    <script type="text/javascript" src="<?php echo JWB; ?>jquery.2.4.js"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR; ?>node_modules/metro-dist/js/metro.js"></script>
    <script type="text/javascript" src="js/updater.js"></script>
    <script type="text/javascript" src="js/gui.js"></script>

    <style>
        .login-form {
            width: 25rem;
            min-height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
            border-radius: 2px;
        }
    </style>

    <script type="text/javascript">
      $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });

            $('.container').addClass('flex-grid')
            $('.mVisitor .controls').addClass('input-control text')
            $('#counter').addClass('button primary')

            fitCenter();

            function fitCenter() {
              var elm = $('.fitCenter')
              $.each(elm, (i, v) => {
                var e = $(v),
                ew = e.width(),
                eh = e.height(),
                p = e.parent(),
                pw = p.width(),
                ph = p.height(),
                pt = p.offset().top,
                pl = p.offset().left,
                et = pt+(ph/2)-(eh/2),
                pl = pl+(pw/2)-(ew/2)
                e.offset({top: et, left:pl})
              })
            }
        });
    </script>

  </head>
  <body class="bg-darkBlue">
    <div id="particles-js"></div>
    <div class="login-form fitCenter padding20 block-shadow">
      <?php if ($_GET['p'] == 'visitor') { ?>
        <div class="mVisitor">
          <?php echo $main_content; ?>
        </div>
      <?php } else { ?>
        <form action="index.php?p=login" method="post">
            <h1 class="text-light"><?php echo __('Librarian LOGIN'); ?></h1>
            <hr class="thin"/>
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="user_login"><?php echo __('Login Username') ?>:</label>
                <input type="text" name="userName" id="user_login">
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="user_password"><?php echo __('Password') ?>:</label>
                <input type="password" name="passWord" id="user_password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" name="logMeIn" class="button info block-shadow-info"><?php echo __('Login') ?></button>
                <a href="index.php" class="button link"><?php echo __('Home') ?></a>
            </div>
        </form>
      <?php } ?>
    </div>
    <?php
    if ($sysconf['template']['particel']) {
      echo '<script type="text/javascript" src="'.THEME_DIR.'assets/js/particles.min.js"></script>';
      echo '<script type="text/javascript" src="'.THEME_DIR.'assets/js/particel.js"></script>';
    }
     ?>
</body>
</html>
