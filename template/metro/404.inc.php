<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-25T08:15:47+07:00
# @Email:  ido.alit@gmail.com
# @Filename: 404.php
# @Last modified by:   user
# @Last modified time: 2017-11-25T09:29:49+07:00

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <style media="screen">
      html, body, ._404 {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      ._404 {
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
      }
      ._404_wraper {
        background: #fff;
        padding: 20px 40px 40px 40px;
        border-radius: 2px;
        box-shadow: 1px 2px 3px rgba(0,0,0,0.3);
      }
      ._404_header {
        font-size: 200px;
      }
      ._404_content {}
      ._404_footer {}
    </style>
  </head>
  <body>
    <?php echo $main_content; ?>
  </body>
</html>
