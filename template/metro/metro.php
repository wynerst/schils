<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-17T20:21:18+07:00
# @Email:  ido.alit@gmail.com
# @Filename: metro.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T11:30:33+07:00


class Metro
{

  private $db;
  private $tiles = array();

  function __construct($obj_db)
  {
    $this->db = $obj_db;
  }

  /**
  * Tile size
  *
  * This method must be call before other.
  * @param $size [square|small|wide|large]
  */
  public function tile($size = null)
  {
    $index = count($this->tiles);
    $this->tiles[$index]['size'] = $size;

    return $this;
  }

  public function icon($icon = null)
  {
    $index = count($this->tiles) - 1;
    $this->tiles[$index]['icon'] = $icon;

    return $this;
  }

  public function color($color = null)
  {
    $index = count($this->tiles) - 1;
    $this->tiles[$index]['color'] = $color;

    return $this;
  }

  public function bg_image($value='')
  {
    # code...
  }

  public function label($label = null)
  {
    $index = count($this->tiles) - 1;
    $this->tiles[$index]['label'] = $label;

    return $this;
  }

  public function link($link = null)
  {
    $index = count($this->tiles) - 1;
    $this->tiles[$index]['link'] = $link;

    return $this;
  }

  public function badge($badge)
  {
    # code...
  }

  public function icon_image($image = null)
  {
    $index = count($this->tiles) - 1;
    $this->tiles[$index]['icon_image'] = $image;

    return $this;
  }

  public function create()
  {

    $create = '';
    foreach ($this->tiles as $tile) {

      $class  = is_null($tile['size']) ? 'tile' : 'tile-' . $tile['size'];
      $class .= is_null($tile['color']) ? ' bg-blue' : ' bg-' . $tile['color'];
      $icon  = isset($tile['icon']) ? $tile['icon'] : ' ';
      $label = isset($tile['label']) ? $tile['label'] : ' ';
      $link = isset($tile['link']) ? $tile['link'] : '#';

      $create .= '<a href="'.$link.'" class="'.$class.' fg-white" data-role="tile">';
      if (isset($tile['icon_image'])) {
        $create .= '<div class="tile-content">';
        $create .= '<img src="'.$tile['icon_image'].'" class="icon">';
      } else {
        $create .= '<div class="tile-content iconic">';
        $create .= '<span class="icon mif-'.$icon.'"></span>';
      }
      $create .= '</div>';
      $text_color = '';
      if ($tile['color'] == 'white') {
        $text_color = " fg-dark";
      }
      $create .= '<span class="tile-label '.$text_color.'">'.$label.'</span>';
      $create .= '</a>';
    }

    echo $create;
    // clear tiles array
    $this->tiles = array();
    return $this;
  }

}
