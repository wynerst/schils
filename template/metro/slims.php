<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-27T10:00:28+07:00
# @Email:  ido.alit@gmail.com
# @Filename: slims.php
# @Last modified by:   user
# @Last modified time: 2017-11-27T10:54:03+07:00


class Slims
{

  private $db;

  function __construct($dbs)
  {
    $this->db = $dbs;
  }

  public function recommended($value='')
  {
    # code...
  }


  /**
  * Popular title base on loan count
  */
  public function popular($sysconf, $size = 10)
  {
    $query = $this->db->query('SELECT b.biblio_id, b.title, b.image, count(l.loan_id) AS total FROM loan AS l
      LEFT JOIN item AS i ON l.item_code = i.item_code
      LEFT JOIN biblio AS b ON i.biblio_id = b.biblio_id
      GROUP BY b.biblio_id
      ORDER BY total DESC
      LIMIT ' . $size);

    if ($query->num_rows < $size) {
      $query = $this->db->query('SELECT biblio_id, title, image FROM biblio ORDER BY biblio_id DESC LIMIT ' . $size);
    }

    $_return = array();
    while ($data = $query->fetch_assoc()) {

      $image_cover = '';
			if ($sysconf['tg']['type'] == 'minigalnano') {

				if (!empty($data['image'])) {
					$book_image = urlencode($book['image']);
					$image_loc = '../../images/docs/'.$book_image;
				} else {
					$image_loc = '../../images/default/image.png';
				}

				$thumb_url = './lib/minigalnano/createthumb.php?filename='.urlencode($image_loc).'&width=120';
        $data['image_url'] = $thumb_url;
			} else {
        if (empty($data['image'])) {
          $data['image_url'] = './images/default/image.png';
        } else {
          $data['image_url'] = './images/docs/' . $data['image'];
        }
      }

      $_return[] = $data;
    }

    return $_return;
  }
}
