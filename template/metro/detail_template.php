<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-19T06:16:37+07:00
# @Email:  ido.alit@gmail.com
# @Filename: detail_template.php
# @Last modified by:   user
# @Last modified time: 2017-11-26T21:47:57+07:00

ob_start();
?>

<div class="flex-grid detail">
  <div class="row">
    <div class="cell colspan8">
      <h4>{title}</h4>
      <p>{authors}</p>
      <hr>
      <p class="notes">{notes}</p>
      <hr class="margin-bottom-20">
      <div class="row">
        <div class="cell size6">
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Statement of Responsibility'); ?>
            </div>
            <div class="cell size8">
              {sor}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Author(s)'); ?>
            </div>
            <div class="cell size8">
              {authors}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Edition'); ?>
            </div>
            <div class="cell size8">
              {edition}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Call Number'); ?>
            </div>
            <div class="cell size8">
              {call_number}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('ISBN/ISSN'); ?>
            </div>
            <div class="cell size8">
              {isbn_issn}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Subject(s)'); ?>
            </div>
            <div class="cell size8">
              {subjects}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Classification'); ?>
            </div>
            <div class="cell size8">
              {classification}
            </div>
          </div>
          <!-- #1 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Series Title'); ?>
            </div>
            <div class="cell size8">
              {series_title}
            </div>
          </div>

        </div>
        <div class="cell size6">
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('GMD'); ?>
            </div>
            <div class="cell size8">
              {gmd_name}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Language'); ?>
            </div>
            <div class="cell size8">
              {language_name}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Publisher'); ?>
            </div>
            <div class="cell size8">
              {publisher_name}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Publishing Year'); ?>
            </div>
            <div class="cell size8">
              {publish_year}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Publishing Place'); ?>
            </div>
            <div class="cell size8">
              {publish_place}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Collation'); ?>
            </div>
            <div class="cell size8">
              {collation}
            </div>
          </div>
          <!-- #2 -->
          <div class="row">
            <div class="cell size4">
              <?php print __('Specific Detail Info'); ?>
            </div>
            <div class="cell size8">
              {spec_detail_info}
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="cell size12">
          <h5><?php print __('File Attachment'); ?></h5>
        </div>
      </div>
      <div class="row">
        <div class="cell size12">
          {file_att}
        </div>
      </div>

      <div class="row">
        <div class="cell size12">
          <h5><?php print __('Availability'); ?></h5>
        </div>
      </div>
      <div class="row">
        <div class="cell size12">
          {availability}
        </div>
      </div>

    </div>
    <div class="cell colspan4 cover">
      <div class="image-container handing ani">
        {image}
      </div>
    </div>
  </div>
</div>

<?php
$detail_template = ob_get_clean();
