<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-18T15:28:36+07:00
# @Email:  ido.alit@gmail.com
# @Filename: start_screen.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T12:20:34+07:00

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
    <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

    <link rel='shortcut icon' type='image/x-icon' href='webicon.ico' />
    <title><?php echo $page_title; ?></title>

    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-icons.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-responsive.css">
    <link rel="stylesheet" href="<?php echo THEME_DIR; ?>assets/css/style.css">

    <script type="text/javascript" src="<?php echo JWB; ?>jquery.2.4.js"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR; ?>node_modules/metro-dist/js/metro.js"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR; ?>assets/js/skel.min.js"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR; ?>assets/js/script2.js"></script>

    <style media="screen">
      canvas {
        display: block;
        vertical-align: bottom;
      }
      #particles-js {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-image: url("");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50% 50%;
      }
    </style>

  </head>
  <body style="overflow-y: scroll;" id="schils">
      <div data-role="charm" data-position="right" id="charmSearch">
          <h1 class="text-light">Search</h1>
          <hr class="thin"/>
          <br />
          <label style="margin-bottom: 10px">
              <span class="dropdown-toggle drop-marker-light">Simple Search</span>
              <ul class="d-menu" data-role="dropdown">
                  <li><a onclick="setSearchPlace(this)">Simple Search</a></li>
                  <li><a onclick="showCharms('#charmAdvancedSearch')"><?php echo __('Advanced Search'); ?></a></li>
              </ul>
          </label>
          <form name="simplySearchForm" id="simplySearchForm" action="index.php" method="get">
            <div style="margin-top:10px" class="input-control text full-size">
                <input style="margin-top:10px" type="text" id="keyword" name="keywords" placeholder="Search...">
                <button style="margin-top:10px" class="button"><span class="mif-search"></span></button>
            </div>
            <input type="hidden" name="search" value="search" />
          </form>
      </div>

      <div data-role="charm" data-position="right" id="charmAdvancedSearch" style="width:320px">
        <h2 class="text-light"><?php echo __('Advanced Search'); ?></h2>
        <hr class="thin"/>
        <!-- advanced Search -->
        <form name="advSearchForm" id="advSearchForm" action="index.php" method="get">
          <label><?php echo __('Author(s)'); ?></label>
          <div class="input-control text full-size">
              <input type="text" name="author" placeholder="<?php echo __('Author(s)'); ?>">
          </div>

          <label><?php echo __('Subject(s)'); ?></label>
          <div class="input-control text full-size">
              <input type="text" name="subject" placeholder="<?php echo __('Subject(s)'); ?>">
          </div>

          <label><?php echo __('ISBN/ISSN'); ?></label>
          <div class="input-control text full-size">
              <input type="text" name="isbn" placeholder="<?php echo __('ISBN/ISSN'); ?>"/>
          </div>

          <label><?php echo __('GMD'); ?></label>
          <div class="input-control select full-size">
              <select name="gmd"><?php echo $gmd_list; ?></select>
          </div>

          <label><?php echo __('Collection Type'); ?></label>
          <div class="input-control select full-size">
              <select name="colltype"><?php echo $colltype_list; ?></select>
          </div>

          <label><?php echo __('Location'); ?></label>
          <div class="input-control select full-size">
              <select name="location"> <?php echo $location_list; ?></select>
          </div>
          <button type="submit" name="search" value="search" class="button success full-size"><span class="mif-search"></span> Search</button>
        </form>
      </div>

      <div data-role="charm" id="charmSettings" data-position="top">
          <h1 class="text-light">Settings</h1>
          <hr class="thin"/>
          <br />
          <div class="schemeButtons">
              <div class="button square-button tile-area-scheme-dark" data-scheme="dark"></div>
              <div class="button square-button tile-area-scheme-darkBrown" data-scheme="darkBrown"></div>
              <div class="button square-button tile-area-scheme-darkCrimson" data-scheme="darkCrimson"></div>
              <div class="button square-button tile-area-scheme-darkViolet" data-scheme="darkViolet"></div>
              <div class="button square-button tile-area-scheme-darkMagenta" data-scheme="darkMagenta"></div>
              <div class="button square-button tile-area-scheme-darkCyan" data-scheme="darkCyan"></div>
              <div class="button square-button tile-area-scheme-darkCobalt" data-scheme="darkCobalt"></div>
              <div class="button square-button tile-area-scheme-darkTeal" data-scheme="darkTeal"></div>
              <div class="button square-button tile-area-scheme-darkEmerald" data-scheme="darkEmerald"></div>
              <div class="button square-button tile-area-scheme-darkGreen" data-scheme="darkGreen"></div>
              <div class="button square-button tile-area-scheme-darkOrange" data-scheme="darkOrange"></div>
              <div class="button square-button tile-area-scheme-darkRed" data-scheme="darkRed"></div>
              <div class="button square-button tile-area-scheme-darkPink" data-scheme="darkPink"></div>
              <div class="button square-button tile-area-scheme-darkIndigo" data-scheme="darkIndigo"></div>
              <div class="button square-button tile-area-scheme-darkBlue" data-scheme="darkBlue"></div>
              <div class="button square-button tile-area-scheme-lightBlue" data-scheme="lightBlue"></div>
              <div class="button square-button tile-area-scheme-lightTeal" data-scheme="lightTeal"></div>
              <div class="button square-button tile-area-scheme-lightOlive" data-scheme="lightOlive"></div>
              <div class="button square-button tile-area-scheme-lightOrange" data-scheme="lightOrange"></div>
              <div class="button square-button tile-area-scheme-lightPink" data-scheme="lightPink"></div>
              <div class="button square-button tile-area-scheme-grayed" data-scheme="grayed"></div>
          </div>
      </div>

      <div class="tile-area tile-area-scheme-dark fg-white" style="height: 100%; max-height: 100% !important;">

        <div id="particles-js"></div>

          <div class="tile-area-title">
            <div class="listview" data-role="listview">
              <div onclick="window.location.href='index.php'" class="list">
                  <img src="<?php echo THEME_DIR . 'assets/images/logo.png'; ?>" class="list-icon">
                  <span class="list-title"><?php echo $sysconf['library_name']; ?></span>
                  <div class="list-data">
                    <?php echo $sysconf['library_subname']; ?>
                  </div>
              </div>
            </div>
          </div>

          <!-- Account tiles -->
          <?php include_once THEME_DIR . 'parts/account.php'; ?>

          <!-- General tiles -->
          <?php include_once THEME_DIR . 'parts/tiles/general.php'; ?>

          <!-- Class tiles -->
          <?php include_once THEME_DIR . 'parts/tiles/class.php'; ?>

          <!-- New Book tiles -->
          <?php //include_once THEME_DIR . 'parts/tiles/newbook.php'; ?>

          <!-- Promoted tiles -->
          <?php //include_once THEME_DIR . 'parts/tiles/promoted.php'; ?>

          <!-- Social tiles -->
          <?php include_once THEME_DIR . 'parts/tiles/social.php'; ?>

      </div>

      <?php

      if ($sysconf['template']['particel']) {
        echo '<script type="text/javascript" src="'.THEME_DIR.'assets/js/particles.min.js"></script>';
        echo '<script type="text/javascript" src="'.THEME_DIR.'assets/js/particel.js"></script>';
      }

       ?>

  </body>
</html>
