<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-18T15:30:08+07:00
# @Email:  ido.alit@gmail.com
# @Filename: portal.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T11:24:57+07:00

if (isset($_GET['p'])) {
  if ($_GET['p'] == 'show_detail') {
    $page_title = __('Bibliographic Details');
  } elseif ($_GET['p'] == 'member') {
    $page_title = __('Member Area');
  } elseif ($_GET['p'] == 'permintaan_buku') {
    $page_title = __('Formulir Permintaan Buku Perpustakaan');
  } elseif ($_GET['p'] == 'permintaan_buku_view') {
    $page_title = __('Permintaan Buku');
  } elseif ($_GET['p'] == 'suggestion_box_view') {
    $page_title = __('Saran dan Kritik');
  }
}

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
  <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
  <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

  <link rel='shortcut icon' type='image/x-icon' href='webicon.ico' />
  <title><?php echo $page_title; ?></title>

  <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro.css">
  <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-colors.css">
  <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-icons.css">
  <link rel="stylesheet" href="<?php echo THEME_DIR; ?>node_modules/metro-dist/css/metro-responsive.css">
  <link rel="stylesheet" href="<?php echo THEME_DIR; ?>assets/css/style_portal.css">

  <script type="text/javascript" src="<?php echo JWB; ?>jquery.2.4.js"></script>
  <script type="text/javascript" src="<?php echo THEME_DIR; ?>node_modules/metro-dist/js/metro.js"></script>

</head>
<body>
    <div class="container container-main">
        <header class="margin20 no-margin-left no-margin-right">
            <div class="clear-float">
                <div class="place-right">
                    <form name="advSearchForm" id="simplySearchForm" action="index.php" method="get">
                        <div class="input-control text margin20" style="width: 300px">
                            <input type="text" name="keywords" placeholder="Search...">
                            <div class="button-group">
                              <button type="submit" class="button"><span class="mif-search"></span></button>
                              <a  class="button"
                                  onclick="showCharms('#charmAdvancedSearch')"
                                  data-role="hint"
                                  data-hint-mode="2"
                                  data-hint="Advanced Search|Show advanced search form">
                                <span class="mif-menu"></span>
                              </a>
                            </div>
                        </div>
                        <input type="hidden" name="search" value="search" />
                    </form>
                </div>
                <div class="tile-area-title brand">
                  <div class="listview" data-role="listview">
                    <div onclick="window.location.href='index.php'" class="list">
                        <img src="<?php echo THEME_DIR . 'assets/images/logo.png'; ?>" class="list-icon">
                        <span class="list-title"><?php echo $sysconf['library_name']; ?></span>
                        <div class="list-data">
                          <?php echo $sysconf['library_subname']; ?>
                        </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="main-menu-wrapper margin-top-2">
                <ul class="horizontal-menu compact" style="margin-left: -20px">
                    <li><a href="index.php">beranda</a></li>
                    <li><a href="index.php?p=libinfo">info</a></li>
                    <li><a href="index.php?p=help">bantuan</a></li>
                    <li><a href="index.php?p=suggestion_box">kotak saran</a></li>
                    <li><a href="index.php?p=permintaan_buku">usulan buku</a></li>
                    <li><a href="index.php?p=mLibrarian">pustakawan</a></li>
                    <li class="place-right">
                        <a href="#" class="dropdown-toggle">
                          <?php if (utility::isMemberLogin()) { ?>
                          <?php echo $_SESSION['m_name']; ?>
                          <?php } else { ?>
                          <?php echo __('Member Area'); ?>
                          <?php } ?>
                        </a>
                        <ul class="d-menu place-right" data-role="dropdown">
                            <li><a href="index.php?p=member">My Account</a></li>
                            <?php if (utility::isMemberLogin()) { ?>
                            <li><a href="index.php?p=member&logout=1">Keluar</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </header>

        <div class="main-content clear-float">
            <div class="tile-area no-padding margin-bottom-60">

              <!-- result search -->
              <?php if(!isset($_GET['p'])) { ?>

                <h3 class="fg-darkBlue text-light margin5">Hasil Pencarian <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span></h3>

                <div class="panel">
                    <div class="content padding10">
                        <?php echo $search_result_info; ?>
                    </div>
                </div>

                <div class="">
                  <?php echo $main_content; ?>
                </div>

                <!-- <h3 class="fg-orange text-light margin5">Rekomendasi <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span></h3>
                <div class="tile-group no-margin no-padding" style="width: 100%;">
                    <div class="tile-large ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile-wide ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                    <div class="tile-wide ol-transparent" data-role="tile">
                      <div class="fitCenter" data-role="preloader" data-type="metro" data-style="color"></div>
                    </div>
                </div> -->

                <?php
                $popular = $slims->popular($sysconf);
                if (count($popular) >= 7) {
                ?>
                <h3 class="fg-blue text-light margin5">Populer <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span></h3>
                <div class="tile-group no-margin no-padding" style="width: 100%;">
                    <a href="index.php?p=show_detail&id=<?php echo $popular[0]['biblio_id']; ?>" class="tile-large ol-transparent" data-role="tile">
                      <div class="tile-content">
                          <div class="image-container">
                              <div class="frame">
                                <img src="<?php echo $popular[0]['image_url'] ?>" alt="<?php echo $popular[0]['image'] ?>" data-role="fitImage" data-format="square" />
                              </div>
                              <div class="image-overlay bg-blue">
                                  <div class="padding10">
                                    <?php echo $popular[0]['title'] ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[1]['biblio_id']; ?>" class="tile-wide ol-transparent" data-role="tile">
                      <div class="tile-content slide-down-2">
                          <div class="slide">
                              <img src="<?php echo $popular[1]['image_url']; ?>" alt="<?php echo $popular[1]['image'] ?>" data-role="fitImage" data-format="hd" />
                          </div>
                          <div class="slide-over padding10 bg-yellow">
                              <?php echo $popular[1]['title']; ?>
                          </div>
                      </div>
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[2]['biblio_id']; ?>" class="tile ol-transparent" data-role="tile">
                      <img src="<?php echo $popular[2]['image_url'] ?>" alt="<?php echo $popular[2]['image'] ?>" data-role="fitImage" data-format="square" />
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[3]['biblio_id']; ?>" class="tile ol-transparent" data-role="tile">
                      <img src="<?php echo $popular[3]['image_url'] ?>" alt="<?php echo $popular[3]['image'] ?>" data-role="fitImage" data-format="square" />
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[4]['biblio_id']; ?>" class="tile ol-transparent" data-role="tile">
                      <img src="<?php echo $popular[4]['image_url'] ?>" alt="<?php echo $popular[4]['image'] ?>" data-role="fitImage" data-format="square" />
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[5]['biblio_id']; ?>" class="tile ol-transparent" data-role="tile">
                      <img src="<?php echo $popular[5]['image_url'] ?>" alt="<?php echo $popular[5]['image'] ?>" data-role="fitImage" data-format="square" />
                    </a>
                    <a href="index.php?p=show_detail&id=<?php echo $popular[6]['biblio_id']; ?>" class="tile-wide ol-transparent" data-role="tile">
                      <div class="tile-content slide-down-2">
                          <div class="slide">
                              <img src="<?php echo $popular[6]['image_url'] ?>" alt="<?php echo $popular[6]['image'] ?>" data-role="fitImage" data-format="hd" />
                          </div>
                          <div class="slide-over padding10 bg-white">
                              <?php echo $popular[6]['title']; ?>
                          </div>
                      </div>
                    </a>
                </div>
                <?php } ?>

              <?php
                } else {
                  if ($_GET['p'] == 'show_detail') {
              ?>
              <h3 class="fg-steel text-light margin5">
                <a href="javascript: history.back();">
                  <span data-role="hint"
                        data-hint-mode="2"
                        data-hint="Kembali ke hasil pencarian">Hasil Pencarian</span>
                  <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span></a>
                <?php echo $page_title; ?> <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span>
              </h3>
              <?php
                  } else {
              ?>

                <h3 class="fg-darkBlue text-light margin5"><?php echo $page_title; ?> <span class="mif-chevron-right mif-2x" style="vertical-align: top !important;"></span></h3>

              <?php } ?>

                <div class="content padding5">
                  <?php echo $main_content; ?>
                </div>

              <?php } ?>
            </div>
        </div> <!-- End of tiles -->
    </div>

    <footer class="footer">
      <div class="container">
        <hr>
        <div class="bottom-menu-wrapper">
            <ul class="horizontal-menu compact">
                <li><a>&copy; <?php echo date('Y'); ?> Schils</a></li>
                <li class="place-right"><a href="#">Privacy</a></li>
                <li class="place-right"><a href="#">Legal</a></li>
                <li class="place-right"><a href="#">Advertise</a></li>
                <li class="place-right"><a href="#">Help</a></li>
                <li class="place-right"><a href="#">Feedback</a></li>
            </ul>
        </div>
      </div>
    </footer>

    <div data-role="charm" data-position="right" id="charmAdvancedSearch" style="width:320px">
      <h2 class="text-light"><?php echo __('Advanced Search'); ?></h2>
      <hr class="thin"/>
      <!-- advanced Search -->
      <form name="advSearchForm" id="advSearchForm" action="index.php" method="get">
        <label><?php echo __('Author(s)'); ?></label>
        <div class="input-control text full-size">
            <input type="text" name="author" placeholder="<?php echo __('Author(s)'); ?>">
        </div>

        <label><?php echo __('Subject(s)'); ?></label>
        <div class="input-control text full-size">
            <input type="text" name="subject" placeholder="<?php echo __('Subject(s)'); ?>">
        </div>

        <label><?php echo __('ISBN/ISSN'); ?></label>
        <div class="input-control text full-size">
            <input type="text" name="isbn" placeholder="<?php echo __('ISBN/ISSN'); ?>"/>
        </div>

        <label><?php echo __('GMD'); ?></label>
        <div class="input-control select full-size">
            <select name="gmd"><?php echo $gmd_list; ?></select>
        </div>

        <label><?php echo __('Collection Type'); ?></label>
        <div class="input-control select full-size">
            <select name="colltype"><?php echo $colltype_list; ?></select>
        </div>

        <label><?php echo __('Location'); ?></label>
        <div class="input-control select full-size">
            <select name="location"> <?php echo $location_list; ?></select>
        </div>
        <button type="submit" name="search" value="search" class="button success full-size"><span class="mif-search"></span> Search</button>
      </form>
    </div>

    <script type="text/javascript">
      $(document).ready(function () {
        $('.pagingList').addClass('pagination')
        $('.pagingList b').addClass('item current')
        $('.pagingList a').addClass('item')

        fitCenter()
      })

      function fitCenter() {
        var elm = $('.fitCenter')
        $.each(elm, (i, v) => {
          var e = $(v),
          ew = e.width(),
          eh = e.height(),
          p = e.parent(),
          pw = p.width(),
          ph = p.height(),
          pt = p.offset().top,
          pl = p.offset().left,
          et = pt+(ph/2)-(eh/2),
          pl = pl+(pw/2)-(ew/2)
          e.offset({top: et, left:pl})
        })
      }

      function showCharms(id){
          var  charm = $(id).data("charm");
          if (charm.element.data("opened") === true) {
              charm.close();
          } else {
              charm.open();
          }
      }
    </script>
</body>
</html>
