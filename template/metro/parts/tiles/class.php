<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-17T20:16:40+07:00
# @Email:  ido.alit@gmail.com
# @Filename: class.php
# @Last modified by:   user
# @Last modified time: 2017-11-29T12:53:05+07:00

?>

<div class="tile-group three">
    <span class="tile-group-title">Klasifikasi</span>

    <div class="tile-container">
      <?php
      $metro->tile('wide')->color('green')->icon('laptop')->label('Komputer & Referensi Umum')->link('?search=search&keywords=class=0');
      $metro->tile()->color('pink')->icon('rainy')->label('Filsafat')->link('?search=search&keywords=class=1');
      $metro->tile()->color('darkCobalt')->icon('infinite')->label('Agama')->link('?search=search&keywords=class=2');
      $metro->tile()->color('darkTeal')->icon('money')->label('Ilmu-Ilmu Sosial')->link('?search=search&keywords=class=3');
      $metro->tile()->color('yellow')->icon('volume-high')->label('Bahasa')->link('?search=search&keywords=class=4');
      $metro->tile()->color('green')->icon('calculator')->label('Sain & Matematika')->link('?search=search&keywords=class=5');
      $metro->tile()->color('red')->icon('satellite')->label('Teknologi')->link('?search=search&keywords=class=6');
      $metro->tile('small')->color('blue')->icon('dribbble')->link('?search=search&keywords=class=7');
      $metro->tile('small')->color('orange')->icon('file-text')->link('?search=search&keywords=class=8');
      $metro->tile('small')->color('cyan')->icon('library')->link('?search=search&keywords=class=9');
      $metro->create();
      ?>
  </div>
</div>
