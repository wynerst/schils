<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-17T20:11:48+07:00
# @Email:  ido.alit@gmail.com
# @Filename: general.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T11:34:04+07:00

?>

<div class="tile-group three">
    <span class="tile-group-title">General</span>

    <div class="tile-container">

      <?php
      // Home
      $metro->tile()->color('orange')->icon('home')->link('index.php')->label(__('Home'));
      // psbSekolah
      $metro->tile('wide')->color('white')->icon_image(THEME_DIR.'assets/images/logo-psb.png')
            ->link('http://psbsekolah.kemdikbud.go.id/')->label(__('PSBSekolah'));
      // Visitor
      $metro->tile()->color('teal')->icon('organization')->link('index.php?p=visitor')->label('Pengunjung');
      // Usul Buku
      $metro->tile()->color('yellow')->icon('books')->link('index.php?p=permintaan_buku')->label('Usul Buku');
      // Librarian
      $metro->tile('small')->color('green')->icon('users')->link('index.php?p=mLibrarian');
      // Help on Search
      $metro->tile('small')->color('pink')->icon('question')->link('index.php?p=help');
      // Library Information
      $metro->tile('small')->color('darkBlue')->icon('info')->link('index.php?p=libinfo');
      // Member Area
      $metro->tile('small')->color('red')->icon('user')->link('index.php?p=member');
      // Kotak kritik saran
      $metro->tile('wide')->color('blue')->icon('dropbox')->link('index.php?p=suggestion_box')->label('Kotak Kritik Saran');
      // Print tiles
      $metro->create();
      ?>

    </div>
</div>
