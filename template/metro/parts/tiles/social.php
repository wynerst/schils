<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-17T21:00:38+07:00
# @Email:  ido.alit@gmail.com
# @Filename: social.php
# @Last modified by:   user
# @Last modified time: 2017-11-18T12:30:01+07:00

?>

<div class="tile-group two">
    <span class="tile-group-title">Social</span>
    <div class="tile-container">
        <?php
        // Twitter
        $metro->tile()->color('blue')->icon('twitter')->label('Twitter')->link('https://twitter.com');
        // Facebook
        $metro->tile()->color('darkBlue')->icon('facebook')->label('Facebook')->link('https://facebook.com');
        // Youtube
        $metro->tile()->color('red')->icon('youtube')->label('Youtube')->link('https://youtube.com');
        // Linkedin
        $metro->tile('small')->color('indigo')->icon('linkedin')->link('https://linkedin.com');
        // Google+
        $metro->tile('small')->color('red')->icon('google-plus')->link('https://plus.google.com');
        // Print tiles
        $metro->create();
         ?>
    </div>
</div>
