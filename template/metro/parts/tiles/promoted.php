<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-18T11:51:49+07:00
# @Email:  ido.alit@gmail.com
# @Filename: promoted.php
# @Last modified by:   user
# @Last modified time: 2017-11-18T11:57:54+07:00

?>

<div class="tile-group double">
    <span class="tile-group-title">Koleksi Faforite</span>

    <div class="tile-container">

      <?php
      // Home
      $metro->tile()->color('orange')->icon('home')->link('index.php')->label(__('Home'));
      // Library Information
      $metro->tile('small')->color('darkBlue')->icon('info')->link('index.php?p=libinfo');
      // Member Area
      $metro->tile('small')->color('red')->icon('user')->link('index.php?p=member');
      // Librarian
      $metro->tile('small')->color('green')->icon('users')->link('index.php?p=librarian');
      // Help on Search
      $metro->tile('small')->color('pink')->icon('question')->link('index.php?p=help');
      // Visitor
      $metro->tile()->color('teal')->icon('organization')->link('index.php?p=visitor')->label('Pengunjung');
      // Usul Buku
      $metro->tile()->color('yellow')->icon('books')->link('index.php?p=usulbuku')->label('Usul Buku');
      // Kotak kritik saran
      $metro->tile('wide')->color('blue')->icon('dropbox')->link('index.php?p=suggestion_box')->label('Kotak Kritik Saran');
      // Print tiles
      $metro->create();
      ?>

    </div>
</div>
