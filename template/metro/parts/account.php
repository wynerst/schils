<?php
# @Author: Waris Agung Widodo <user>
# @Date:   2017-11-17T20:09:11+07:00
# @Email:  ido.alit@gmail.com
# @Filename: account.php
# @Last modified by:   user
# @Last modified time: 2017-12-14T09:30:46+07:00

?>

<div class="tile-area-controls">
    <button onclick="window.location.href='index.php?p=member'" class="image-button icon-right bg-transparent fg-white bg-hover-dark no-border"><span class="sub-header no-margin text-light">
      <?php if (utility::isMemberLogin()) { ?>
      <?php echo $_SESSION['m_name']; ?>
      <?php } else { ?>
      <?php echo __('Member Area'); ?>
      <?php } ?>
    </span> <span class="icon mif-user"></span></button>
    <button class="square-button bg-transparent fg-white bg-hover-dark no-border" onclick="showCharms('#charmSearch')"><span class="mif-search"></span></button>
    <button class="square-button bg-transparent fg-white bg-hover-dark no-border" onclick="showCharms('#charmSettings')"><span class="mif-cog"></span></button>
    <a href="index.php?p=login" class="square-button bg-transparent fg-white bg-hover-dark no-border"><span class="mif-enter"></span></a>
</div>
